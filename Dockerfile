FROM docker.io/library/php:7.4-fpm

RUN \
    apt-get update && \
    apt-get install -y \
        build-essential \
        && \
    curl -sL https://deb.nodesource.com/setup_lts.x | bash -

RUN \
    apt-get update && \
    apt-get install -y \
        nodejs

RUN \
    apt-get update && \
    apt-get install -y \
        libglu1 \
        libzip-dev \
        zlib1g-dev

RUN \
    apt-get update && \
    apt-get install -y \
        default-mysql-client \
        git \
        unzip \
        wkhtmltopdf \
        zip \
        && \
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN \
    install-php-extensions \
        bcmath \
        gd \
        pdo_mysql \
        xdebug \
        zip \
        @composer

ENV PROJECT_ROOT /opt/project
#ENV NPM_CONFIG_PREFIX ${PROJECT_ROOT}/node_modules_docker

